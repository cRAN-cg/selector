import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SelectorComponent } from './selector/selector.component';
import { SelectorNodeComponent } from './selector-node/selector-node.component';
import { ItemListContainerComponent } from './item-list-container/item-list-container.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectorComponent,
    SelectorNodeComponent,
    ItemListContainerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
