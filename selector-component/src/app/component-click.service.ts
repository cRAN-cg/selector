import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentClickService {

  private componentClickedSource = new Subject<Boolean>();

  componentClicked$ = this.componentClickedSource.asObservable();

  componentClicked(isClicked: boolean) {
    this.componentClickedSource.next(isClicked);
  }
}
