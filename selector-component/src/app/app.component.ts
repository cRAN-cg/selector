import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { SelectorNode } from './common/selector-node';
import { Nodes } from './common/mock-selector-nodes';
import { ItemSelectionService } from './item-selection.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'selector-component';
  trigger = false;
  data: SelectorNode[];
  savedNodeList: SelectorNode[];
  temporaryNodeList: SelectorNode[];

  constructor(private eRef: ElementRef, private saveNodeListService: ItemSelectionService) {
    saveNodeListService.nodeListSelected$.subscribe(nodeList => this.temporaryNodeList = nodeList);
    saveNodeListService.saveNodeListSource$.subscribe(save => this.saveNodeList());
  }
  @HostListener('document:click', ['$event'])
  clickout(event) {

    let eventTarget = (<Element>event.target);
    if (eventTarget.textContent === 'Trigger') {
      return;
    }
    if (eventTarget.className &&
      eventTarget.className.includes('selector')) {
      if (this.eRef.nativeElement.contains(event.target) || eventTarget.className.includes('remove')) {
      }
    } else {
      this.trigger = false;
    }
  }
  onTriggerClick() {
    this.trigger = !this.trigger;
  }

  ngOnInit(): void {
    this.data = Nodes.data;
  }

  private saveNodeList(){
    this.savedNodeList = this.temporaryNodeList;
    console.log(this.savedNodeList);
    this.onTriggerClick();
  }

}
