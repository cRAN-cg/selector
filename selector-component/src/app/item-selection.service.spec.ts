import { TestBed } from '@angular/core/testing';

import { ItemSelectionService } from './item-selection.service';

describe('ItemSelectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemSelectionService = TestBed.get(ItemSelectionService);
    expect(service).toBeTruthy();
  });
});
