export class SelectorNode {
    label?: string;
    data?: any;
    children?: SelectorNode[];
    expanded?: boolean;
}
