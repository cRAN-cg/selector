import { Component, OnInit, Input } from '@angular/core';
import { SelectorNode } from '../common/selector-node';
import { ItemSelectionService } from '../item-selection.service';

@Component({
  selector: 'app-selector-node',
  templateUrl: './selector-node.component.html',
  styleUrls: ['./selector-node.component.css']
})
export class SelectorNodeComponent implements OnInit {

  @Input() node: SelectorNode;
  constructor(private nodeSelectionService: ItemSelectionService) {
    // nodeSelectionService.nodeItemSelected$.subscribe(
    //   node => console.log(node)
    // );

    // For removing element from list and changing the class of the unselected item;
  }

  ngOnInit() {
    this.node.expanded = false;
  }

  public toggle(event: Event) {
    if (this.node.expanded) {
      this.collapse(event);
    } else {
      this.expand(event);
    }
  }

  public onNodeClick(event: Event) {
    let eventTarget = (<Element>event.target);

    if (eventTarget.className && eventTarget.className.indexOf('selector-node__toggler') === 0) {
      return;
    }
    this.nodeSelectionService.putItemInList(this.node);
  }

  private expand(event: Event) {
    this.node.expanded = true;
  }

  private collapse(event: Event) {
    this.node.expanded = false;
  }

}
