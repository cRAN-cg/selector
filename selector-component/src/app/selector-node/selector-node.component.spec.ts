import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectorNodeComponent } from './selector-node.component';

describe('SelectorNodeComponent', () => {
  let component: SelectorNodeComponent;
  let fixture: ComponentFixture<SelectorNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectorNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectorNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
