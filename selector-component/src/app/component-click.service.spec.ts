import { TestBed } from '@angular/core/testing';

import { ComponentClickService } from './component-click.service';

describe('ComponentClickService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComponentClickService = TestBed.get(ComponentClickService);
    expect(service).toBeTruthy();
  });
});
