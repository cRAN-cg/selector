import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { SelectorNode } from '../common/selector-node';
import { Nodes } from '../common/mock-selector-nodes';
import { ItemSelectionService } from '../item-selection.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent implements OnInit {

  @Input() value: SelectorNode[];
  filteredNodes: SelectorNode[];
  _value: SelectorNode[];

  constructor(private eRef: ElementRef, private selectedListNodesService: ItemSelectionService) {
   }

  ngOnInit() {
    this._value = this.value;
  }

  onFilter(event) {
    let filterValue = event.target.value;
    if (filterValue === '') {
      this.filteredNodes = null;
    } else {
      this.filteredNodes = [];
      const filterText = filterValue.toLowerCase();
      let nodes = [...this.value];
      for (let node of nodes) {
        let copyNode = { ...node };
        if (this.findFilteredNodes(filterText, copyNode)) {
          this.filteredNodes.push(copyNode);
        }
      }
    }
  }

  findFilteredNodes(filterText: string, node: SelectorNode) {
    if (node) {
      let matched = false;
      if (node.children) {
        let childNodes = [...node.children];
        node.children = [];
        for (let childNode of childNodes) {
          let copyChildNode = { ...childNode };
          if (copyChildNode.label.toLowerCase().includes(filterText)) {
            matched = true;
            node.children.push(copyChildNode);
          }
        }
      }
      if (matched) {
        return true;
      }
    }
  }

  getRootNode() {
    let a = this._getRootNode();
    return a;
  }

  _getRootNode() {
    return this.filteredNodes ? this.filteredNodes : this.value;
  }

  triggerSave(event: Event) {
    this.selectedListNodesService.saveNodeList(true);
  }
}
