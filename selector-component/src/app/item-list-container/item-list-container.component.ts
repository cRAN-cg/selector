import { Component, OnInit } from '@angular/core';
import { ItemSelectionService } from '../item-selection.service';
import { SelectorNode } from '../common/selector-node';

@Component({
  selector: 'app-item-list-container',
  templateUrl: './item-list-container.component.html',
  styleUrls: ['./item-list-container.component.css']
})
export class ItemListContainerComponent implements OnInit {

  nodeList: SelectorNode[] = [];
  constructor(private itemSelectionService: ItemSelectionService) {
    itemSelectionService.nodeItemSelected$.subscribe(
      node => this.addNodeToList(node)
    );
  }

  ngOnInit() {
  }

  private addNodeToList(node) {
    if (node.children) {
      node.children.forEach(nodeChild => {
        this.addNodeToList(nodeChild);
      });
    } else {
      if (this.nodeList.find(nodeItem => nodeItem.label === node.label)) {
        return;
      }
      this.nodeList.push(node);
    }
    this.updateNodeListGlobally();
  }

  removeNodeFromList(event: Event, index: number, node: SelectorNode) {
    const length = this.nodeList.length;
    if (index === 0) {
      this.nodeList = this.nodeList.slice(1);
    } else if (index === length - 1) {
      this.nodeList = this.nodeList.slice(0, length - 1);
    } else {
      this.nodeList = [
        ...this.nodeList.slice(0, index),
        ...this.nodeList.slice(index + 1)
      ];
    }

    this.updateNodeListGlobally();
  }

  private updateNodeListGlobally() {
    this.itemSelectionService.updateNodeList(this.nodeList);
  }

}
