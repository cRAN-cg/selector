import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SelectorNode } from './common/selector-node';

@Injectable({
  providedIn: 'root'
})
export class ItemSelectionService {
  private nodeItemSelectedSource = new Subject<SelectorNode>();
  private nodeListSelectedSource = new Subject<SelectorNode[]>();
  private saveNodeListSource = new Subject<boolean>();

  nodeItemSelected$ = this.nodeItemSelectedSource.asObservable();

  nodeListSelected$ = this.nodeListSelectedSource.asObservable();

  saveNodeListSource$ = this.saveNodeListSource.asObservable();

  putItemInList(node: SelectorNode) {
    this.nodeItemSelectedSource.next(node);
  }
  updateNodeList(nodeList: SelectorNode[]) {
    this.nodeListSelectedSource.next(nodeList);
  }
  saveNodeList(save: boolean){
    this.saveNodeListSource.next(save);
  }
}
